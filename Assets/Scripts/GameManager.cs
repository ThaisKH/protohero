﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
public enum GameStates
{
    playing,
    end
}
public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public GameStates gameState;
    public GameObject Ball;
    public Text Points;
    public Text Record;
    public Text FinalPoints;
    public Text FinalRecord;
    public GameObject GameOverPanel;
    
    [HideInInspector]
    public float points = 0;
    [HideInInspector]
    public float record = 0;
    [HideInInspector]
    public int errors = 0;
    public bool End = false;
    public float Multiplier = 1f;
    
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        gameState = GameStates.playing;
    }

    void Start()
    {
        StartCoroutine(Timer());
        GameOverPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        gameStateUpdate();
        Points.text = points.ToString();
        if (points > record)
        {
            record = points;
        }
        Record.text = record.ToString();
        FinalPoints.text = "Your Score: " + points;
        FinalRecord.text = "Record: " + record;
        UpdateDifficulty(250,1.5f);
        UpdateDifficulty(500,2f);
        UpdateDifficulty(750,2.5f);
        UpdateDifficulty(1000,3f);
        Debug.Log(Multiplier);
        if (errors >= 20 * Multiplier)
        {
            End = true;
        }
    }
    
    void gameStateUpdate()
    {
        switch (gameState)
        {
            case GameStates.end:
                setGameInEnd();
                break;
            default:
                setGameInPlaying();
                break;
        }
    }

    private void setGameInPlaying()
    {
        Time.timeScale = 1;
        GameOverPanel.SetActive(false);
    }

    private void setGameInEnd()
    {
        Time.timeScale = 0;
        GameOverPanel.SetActive(true);
    }
    
    public GameStates GetGameState()
    {
        return this.gameState;
    }

    public void SetGameState(GameStates gm)
    {
        this.gameState = gm;
        gameStateUpdate();
    }

    public void PlayAgain()
    {
        SetGameState(GameStates.playing);
        points = 0;
        errors = 0;
        End = false;
        StartCoroutine(Timer());
        
    }
    public void Quit()
    {
        Application.Quit();
    }

    public void UpdateDifficulty(int p, float m)
    {
        if (points > p)
        {
            Multiplier = m;
        }
    }

    public IEnumerator Timer()
    {
        int line = Random.Range(0,5);
        Generator(line);
        
        yield return new WaitForSeconds(1f);
        if (End)
        {
            SetGameState(GameStates.end);
        }
        else
        {
            StartCoroutine(Timer());
        }
    }

    public void Generator(int line)
    {
        Instantiate(Ball, new Vector3(-24.1f, 0.243f, line*3f), transform.rotation);
    }
}
