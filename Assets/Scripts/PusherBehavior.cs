﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PusherBehavior : MonoBehaviour
{
    public KeyCode Key;
    private bool m_Active;
    private GameObject m_Ball;
    public Material Default;
    public Material Pushed;

    private MeshRenderer m_MeshRenderer;
    // Start is called before the first frame update
    void Start()
    {
        m_MeshRenderer = this.gameObject.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager._instance.gameState == GameStates.playing)
        {
            m_MeshRenderer.material = Default;
            if (Input.GetKeyDown(Key) && m_Active)
            {
                Destroy(m_Ball);
                GameManager._instance.points += (1*GameManager._instance.Multiplier);
                m_MeshRenderer.material = Pushed;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        m_Active = true;
        if (other.gameObject.CompareTag("Ball"))
        {
            m_Ball = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        m_Active = false;
    }
}
